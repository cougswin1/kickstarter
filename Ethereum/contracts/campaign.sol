pragma solidity ^0.4.17;

//create contract that creates other contracts
contract CampaignFactory {
    address[] public deployedCampaigns;

    function createCampaign(uint minimum) public {

        address newCampaign = new Campaign(minimum, msg.sender);
        deployedCampaigns.push(newCampaign);

    }
    function getDeployedCampaigns() public view returns (address[]) {
        return deployedCampaigns;
    }

}

contract Campaign {
    //creating the "idea" or type of a struct not actually creating the instance or function.
    struct Request{
        string description;
        uint value;
        address recipient;
        bool complete;
        uint approvalCount;
        mapping(address => bool) approvals; //reference type doesnt need to be listed in request below - line 39
    }
    Request[] public requests; //creating instance of request struct
    address public manager;
    uint public minimumContribution;
    mapping(address => bool) public approvers; // this is much less expensive
    uint public approversCount;

    modifier restricted() {
        require(msg.sender==manager);
        _;

    }
    //sets up campaign function and sets the minimumContribution
    function Campaign(uint minimum, address creator) public {
        manager = creator;
        minimumContribution = minimum;

    }
    //requires senders send minimumContribution
    function contribute() public payable {
        require(msg.value > minimumContribution);

        approversCount++;
        approvers[msg.sender] = true;

    }
    //create new request
    function createRequest(string description, uint value, address recipient) public restricted {

        Request memory newRequest = Request({ //memory state vs. storage
            description: description,
            value: value,
            recipient: recipient,
            complete: false,
            approvalCount: 0

        });


        requests.push(newRequest);

    }
    // allow approvers to vote and approve a request
    function approveRequest(uint index) public {
        Request storage request = requests[index];

        require(approvers[msg.sender]);
        require(!request.approvals[msg.sender]);

        request.approvals[msg.sender] = true;
        request.approvalCount++;

    }

    function finalizeRequest(uint index) public restricted {
        Request storage request = requests[index];

        require(request.approvalCount > (approversCount /2));
        require(!request.complete);

        request.recipient.transfer(request.value);
        request.complete = true;
    }



}
