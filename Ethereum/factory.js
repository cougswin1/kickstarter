import web3 from "./web3";
import CampaignFactory from "./build/CampaignFactory.json";

const instance = new web3.eth.Contract(
  JSON.parse(CampaignFactory.interface),
  "0x84363806f741AFCB64e86f04DF2c2cE91518689e"
);

export default instance;
